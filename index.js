db.fruits.insertMany([
  {
    name : "Apple",
    supplier: "Red Farms Inc.",
    stocks : 20,
    price: 40,
    onSale : true,
  },

  {
    name : "Banana",
    supplier : "Yellow Farms",
    stocks : 15,
    price: 20,
    onSale : true,
  },

  {
    name : "Kiwi",
    supplier : "Green Farming and Canning",
    stocks : 25,
    price: 50,
    onSale : true,
  },

  {
    name : "Mango",
    supplier : "Yellow Farms",
    stocks : 10,
    price: 60,
    onSale : true,
  },
  {
    name : "Dragon Fruit",
    supplier: "Red Farms Inc.",
    stocks : 10,
    price: 60,
    onSale : true,
  },

])

//--------------------------------------------

db.fruits.aggregate([

    {$match:{$and:[{supplier:{"Yellow Farms"}},{price:{$lt:50}}]}},
    {$count: "priceYellowLess50"}

])

db.fruits.aggregate([

    {$match: {price:{$lt:30}}},
    {$count: "priceAllLess30"}

])

db.fruits.aggregate([
    
    //Apple,Dragonfruit
    {$match: {supplier:"Yellow Farms"}},
    {$group:{_id:"priceYellowAve",avgPrice:{$avg:"$price"}}}

])

db.fruits.aggregate([

    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"maxPrice", maxPrice: {$max: "$price"}}}

])

db.fruits.aggregate([

    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"minPrice", minPrice: {$min: "$price"}}}

])